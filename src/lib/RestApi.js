import RestClientFactory from "../support/RestClient";
import RestResource from "./RestResource";

class RestApi {

    constructor(apiUrl) {

        this.apiUrl = RestApi.normalizeApiUrl(apiUrl);
        this.clientFactory = new RestClientFactory(this.apiUrl);

        this.restResources = {};
    }

    setHeaders(headers) {
        this.clientFactory.setHeaders(headers);
    }

    getResources(callback, errorCallback) {

        this.clientFactory.follow(this.apiUrl, (result) => {

            const resources = [];

            Object.keys(result._links)
                .filter(key => key !== 'profile')
                .forEach(key => resources.push(key));

            callback(resources);
        }, errorCallback);
    }

    getResource(resourceName) {

        if (this.restResources[resourceName]) {
            return this.restResources[resourceName];
        }

        const restClient = this.clientFactory.newResourceClient(resourceName);
        const restResource = new RestResource(this, restClient, resourceName);

        this.restResources[resourceName] = restResource;
        return restResource;
    }

    static normalizeApiUrl(apiUrl) {
        return (apiUrl.slice(-1) === '/') ? apiUrl : apiUrl + '/';
    }
}

export default RestApi;