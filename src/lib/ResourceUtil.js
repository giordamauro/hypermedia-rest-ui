import Pluralize from "pluralize";

class ResourceUtil {

    static STRING_ARRAY_SCHEMA = {
        "ui": "string-array",
        "type":"object",
        "properties": {
            "value": {
                "title": "Value",
                "readOnly" : false,
                "type": "string"
            }
        }
    };

    static getNonObjectHeaders(schemaProperties) {

        const headers = [];

        Object.entries(schemaProperties)
            .forEach(([key, value]) => {

                if (value.type !== 'object' && value.type !== 'array' && !ResourceUtil.isReference(value)) {

                    const propertyDetails = ResourceUtil.getPropertyDetails(value.description);

                    const order = (propertyDetails && propertyDetails.order) ? propertyDetails.order - 1 : 999;

                    const header = {
                        key: key,
                        value: value,
                        type: 'value',
                        details: propertyDetails,
                        subSchema: null,
                        refName: null,
                        order: order
                    };
                    headers.push(header);
                }
            });

        headers.sort((a, b) => a.order > b.order);

        return headers;
    }

    static getHeaders(schema) {

        const schemaProperties = schema.properties;
        const definitions = schema.definitions;
        const descriptors = schema.descriptors;

        const headers = [];
        const objHeaders = [];
        const refHeaders = [];

        Object.entries(schemaProperties)
            .forEach(([key, value]) => {

                const propertyDetails = ResourceUtil.getPropertyDetails(value.description);

                const order = (propertyDetails && propertyDetails.order) ? propertyDetails.order - 1 : 999;
                const defaultValue = (propertyDetails && propertyDetails.default) ? propertyDetails.default : null;

                if (defaultValue) {
                    value.default = defaultValue;
                }

                let stack = headers;
                let type = 'value';
                let singular = true;
                let subSchema = null;
                let refName = null;

                if (value.type === 'object' || value.type === 'array') {

                    subSchema = ResourceUtil.getDefinition(definitions, value);
                    subSchema.title = Pluralize(value.title, 1);
                    subSchema.definitions = definitions;
                    subSchema.descriptors = descriptors;

                    if (propertyDetails && propertyDetails.descriptions) {
                        ResourceUtil.assignSubSchemaDescriptions(subSchema, propertyDetails.descriptions);
                    }

                    stack = objHeaders;
                    type = 'object';
                    singular = (value.type !== 'array');
                }
                else if (ResourceUtil.isReference(value)) {

                    refName = (propertyDetails && propertyDetails.profile) ? propertyDetails.profile :
                        ResourceUtil.getFieldRepresentation(descriptors, key);

                    stack = refHeaders;
                    type = 'ref';
                    singular = (Pluralize(key) !== key);
                }

                const header = {
                    key: key,
                    value: value,
                    type: type,
                    details: propertyDetails,
                    subSchema: subSchema,
                    refName: refName,
                    singular: singular,
                    order: order
                };

                stack.push(header);
            });

        headers.sort((a, b) => a.order > b.order);
        objHeaders.sort((a, b) => a.order > b.order);
        refHeaders.sort((a, b) => a.order > b.order);

        return headers.concat(objHeaders).concat(refHeaders);
    }

    static isReference(headerValue) {
        return headerValue.type === "string" && headerValue.format === "uri";
    }

    static getSelfHref(resource) {

        return (resource._links && resource._links.self) ? resource._links.self.href : null;
    }

    static getFieldHref(resource, fieldName) {
        return (resource._links && resource._links[fieldName]) ? resource._links[fieldName].href : null;
    }

    static getSelfId(resource) {

        const selfLink = ResourceUtil.getSelfHref(resource);
        return (selfLink) ? selfLink.split("/").pop() : null;
    }

    static getPropertyDetails(description) {

        if (description) {

            if(typeof description === 'object') {

                if(!description.description) {
                    description.description = "";
                }

                return description;
            }

            let strDescription = description;
            if(description.charAt(0) === '\'' && description.charAt(description.length - 1) === '\'') {
                strDescription = description.substring(1, description.length - 1);
            }

            try {
                return JSON.parse(strDescription);
            } catch (e) {
                // do nothing
            }
        }

        return null;
    }

    static getDefinition(definitions, headerValue) {

        if(headerValue.type === 'array' && headerValue.items.type === 'string') {
            return ResourceUtil.STRING_ARRAY_SCHEMA;
        }

        const ref = (headerValue.type === 'object') ? headerValue.$ref : headerValue.items.$ref;
        const fieldDefinition = ref.replace("#/definitions/", '');

        return definitions[fieldDefinition];
    }

    static getFieldRepresentation(descriptors, fieldName) {

        const rt = descriptors.filter(descriptor => descriptor.name === fieldName)
            .map(descriptor => descriptor.rt);

        return (rt.length === 1) ?
            rt[0].split("#")[0].split('/').pop()
            :
            Pluralize(fieldName);
    }

    static assignSubSchemaDescriptions(subSchema, descriptions) {

        const properties = subSchema.properties;

        Object.keys(properties).forEach(property => {
            const description = descriptions[property];

            if (description) {
                properties[property].description = description;
            }
        });
    }

    static isEditable(header) {

        const propertyDetails = header.details;
        if (propertyDetails) {

            if (propertyDetails.readOnly || (propertyDetails.hasOwnProperty("editable") && !propertyDetails.editable)) {
                return false;
            }
        }
        return true;
    }
}

export default ResourceUtil;