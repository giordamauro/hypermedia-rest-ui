class RestResourceItem {

    constructor(restResource, resourceId) {

        this.restResource = restResource;
        this.resourceId = resourceId;
    }

    getRestResource() {
        return this.restResource;
    }

    getResourceId() {
        return this.resourceId;
    }

    update(resourceData, callback, errorCallback) {
        this.restResource.restClient.update(this.resourceId, resourceData, callback, errorCallback);
    }

    patch(resourceData, callback, errorCallback) {
        this.restResource.restClient.patch(this.resourceId, resourceData, callback, errorCallback);
    }

    remove(callback, errorCallback) {
        this.restResource.restClient.remove(this.resourceId, callback, errorCallback);
    }

    get(callback, errorCallback) {
        this.restResource.restClient.getOne(this.resourceId, callback, errorCallback);
    }

    updateRelationship(fieldName, linkList, callback, errorCallback) {
        this.restResource.restClient.updateRelationship(this.resourceId, fieldName, linkList, callback, errorCallback);
    }
}

export default RestResourceItem;