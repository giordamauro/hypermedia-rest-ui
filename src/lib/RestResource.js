class RestResource {

    constructor(restApi, restClient, resourceName) {

        this.restApi = restApi;
        this.restClient = restClient;
        this.resourceName = resourceName;
    }

    getRestApi() {
        return this.restApi;
    }

    getRestClient() {
        return this.restClient;
    }

    getResourceName() {
        return this.resourceName;
    }

    getResourceMetadata(callback, errorCallback) {

        if(this.metadata) {
            callback(this.metadata);
            return;
        }

        const restResource = this;
        this.restClient.getAlpsAndJsonSchema(metadata => {

                restResource.metadata = metadata;
                callback(metadata);
            }, errorCallback);
    }

    getJsonSchema(callback, errorCallback) {
        this.restClient.getJsonSchema(callback, errorCallback);
    }

    getAlps(callback, errorCallback) {
        this.restClient.getAlps(callback, errorCallback);
    }

    getPaged(pageNumber, pageSize, callback, errorCallback) {
        this.restClient.getPagedList(pageNumber, pageSize, callback, errorCallback);
    }

    getPagedQuery(pageNumber, pageSize, query, callback, errorCallback) {
        this.restClient.getPagedListQuery(pageNumber, pageSize, query, callback, errorCallback);
    }

    create(resourceData, callback, errorCallback) {
        this.restClient.create(resourceData, callback, errorCallback);
    }
}

export default RestResource;