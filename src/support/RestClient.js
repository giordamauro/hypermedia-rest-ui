class RestClient {

    static handleResponse(response) {

        const json = response.json(); // there's always a body
        if (response.status >= 200 && response.status < 300) {
            return json;
        } else {
            return json.then(err => {
                const error = {status: response.status, message: err};
                throw error;
            });
        }
    }

    static handleException(result) {

        if (result.json) {
            result.json().then(json => {
                throw json
            });
        }
        else if (result.text) {

            result.text().then(text => {
                throw text
            });
        }
        else throw new Error(result.statusText);
    }

    static treatException(error, errorCallback) {
        if (errorCallback) {
            errorCallback(error);
        }
        else {
            console.log(error);

            let readableError = (typeof error === 'object') ? JSON.stringify(error, null, 2) : error;
            alert(readableError);
        }
    }
}

class ResourceRestClient {

    constructor(apiUrl, headers, resourceName) {

        this.apiUrl = apiUrl;
        this.headers = headers;
        this.resourceName = resourceName;
    }

    getOne(resourceId, callback, errorCallback) {

        fetch(this.apiUrl + this.resourceName + '/' + resourceId, {
            method: 'GET',
            headers: this.headers
        })
            .then(RestClient.handleResponse, RestClient.handleException)
            .then(jsonResult => callback(jsonResult))
            .catch(error => RestClient.treatException(error, errorCallback));
    }

    create(resourceData, callback, errorCallback) {

        let headers = JSON.parse(JSON.stringify(this.headers));

        fetch(this.apiUrl + this.resourceName, {
            method: 'POST',
            headers: Object.assign(headers, {'Content-type': 'application/json'}),
            body: JSON.stringify(resourceData)
        })
            .then(RestClient.handleResponse, RestClient.handleException)
            .then(jsonResult => callback(jsonResult))
            .catch(error => RestClient.treatException(error, errorCallback));
    }

    update(resourceId, resourceData, callback, errorCallback) {

        let headers = JSON.parse(JSON.stringify(this.headers));

        fetch(this.apiUrl + this.resourceName + '/' + resourceId, {
            method: 'PUT',
            headers: Object.assign(headers, {'Content-type': 'application/json'}),
            body: JSON.stringify(resourceData)
        })
            .then(RestClient.handleResponse, RestClient.handleException)
            .then(jsonResult => callback(jsonResult))
            .catch(error => RestClient.treatException(error, errorCallback));
    }

    patch(resourceId, resourceData, callback, errorCallback) {

        let headers = JSON.parse(JSON.stringify(this.headers));

        fetch(this.apiUrl + this.resourceName + '/' + resourceId, {
            method: 'PATCH',
            headers: Object.assign(headers, {'Content-type': 'application/json'}),
            body: JSON.stringify(resourceData)
        })
            .then(RestClient.handleResponse, RestClient.handleException)
            .then(jsonResult => callback(jsonResult))
            .catch(error => RestClient.treatException(error, errorCallback));
    }

    updateRelationship(resourceId, relationship, linkList, callback, errorCallback) {

        let headers = JSON.parse(JSON.stringify(this.headers));

        fetch(this.apiUrl + this.resourceName + '/' + resourceId + '/' + relationship, {
            method: 'PUT',
            headers: Object.assign(headers, {'Content-type': 'text/uri-list'}),
            body: linkList.join("\n")
        })
            .then(result => callback())
            .catch(error => RestClient.treatException(error, errorCallback));
    }

    remove(resourceId, callback, errorCallback) {

        fetch(this.apiUrl + this.resourceName + '/' + resourceId, {
            method: 'DELETE',
            headers: this.headers
        }).then(jsonResult => callback())
            .catch(error => RestClient.treatException(error, errorCallback));
    }

    getPagedList(page, size, callback, errorCallback) {

        fetch(this.apiUrl + this.resourceName + '?page=' + page + '&size=' + size, {
            method: 'GET',
            headers: this.headers
        })
            .then(RestClient.handleResponse, RestClient.handleException)
            .then(jsonResult => callback(jsonResult))
            .catch(error => RestClient.treatException(error, errorCallback));
    }

    getPagedListQuery(page, size, query, callback, errorCallback) {

        fetch(this.apiUrl + this.resourceName + '?page=' + page + '&size=' + size + '&' + query, {
            method: 'GET',
            headers: this.headers
        })
            .then(RestClient.handleResponse, RestClient.handleException)
            .then(jsonResult => callback(jsonResult))
            .catch(error => RestClient.treatException(error, errorCallback));
    }

    getAlps(callback, errorCallback) {

        let headers = JSON.parse(JSON.stringify(this.headers));

        fetch(this.apiUrl + 'profile/' + this.resourceName, {
            method: 'GET',
            headers: Object.assign(headers, {'Accept': 'application/alps+json'}),
        })
            .then(RestClient.handleResponse, RestClient.handleException)
            .then(jsonResult => callback(jsonResult))
            .catch(error => RestClient.treatException(error, errorCallback));
    }

    getJsonSchema(callback, errorCallback) {

        let headers = JSON.parse(JSON.stringify(this.headers));

        fetch(this.apiUrl + 'profile/' + this.resourceName, {
            method: 'GET',
            headers: Object.assign(headers, {'Accept': 'application/schema+json'}),
        })
            .then(RestClient.handleResponse, RestClient.handleException)
            .then(jsonResult => callback(jsonResult))
            .catch(error => RestClient.treatException(error, errorCallback));
    }

    getAlpsAndJsonSchema(callback, errorCallback) {

        const headers = {};
        Object.assign(headers, this.headers);

        const jsonSchemaFetch = fetch(this.apiUrl + 'profile/' + this.resourceName, {
            method: 'GET',
            headers: Object.assign(headers, {'Accept': 'application/schema+json'}),
        });

        const alpsFetch = fetch(this.apiUrl + 'profile/' + this.resourceName, {
            method: 'GET',
            headers: Object.assign(headers, {'Accept': 'application/alps+json'}),
        });

        Promise.all([jsonSchemaFetch, alpsFetch])
            .then(results => Promise.all(results.map(response => response.json())))
            .then(jsonResults => {

                const metadata = {};
                metadata.jsonSchema = jsonResults[0];
                Object.assign(metadata, jsonResults[1]);

                callback(metadata);
            })
            .catch(error => RestClient.treatException(error, errorCallback));
    }

    follow(href, callback, errorCallback) {

        fetch(href, {
            method: 'GET',
            headers: this.headers
        })
            .then(RestClient.handleResponse, RestClient.handleException)
            .then(jsonResult => callback(jsonResult))
            .catch(error => RestClient.treatException(error, errorCallback));
    }
}

class RestClientFactory {

    constructor(apiUrl) {
        this.apiUrl = apiUrl;
        this.headers = {};
    }

    setHeaders(headers) {
        this.headers = headers;
    }

    newResourceClient(resourceName) {
        return new ResourceRestClient(this.apiUrl, this.headers, resourceName);
    }

    follow(href, callback, errorCallback) {

        fetch(href, {
            method: 'GET',
            headers: this.headers
        })
            .then(RestClient.handleResponse, RestClient.handleException)
            .then(jsonResult => callback(jsonResult))
            .catch(error => RestClient.treatException(error, errorCallback));
    }
}

module.exports = RestClientFactory;