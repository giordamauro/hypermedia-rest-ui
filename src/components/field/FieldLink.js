import React, {Component} from "react";
import {Button, Glyphicon, Modal} from "react-bootstrap";

import ResourceForm from "../form/ResourceForm";
import FieldBreadcrumb from "./FieldBreadcrumb";

class FieldLink extends Component {

    constructor(props) {
        super(props);

        this.onClick = this.onClick.bind(this);
        this.closeModal = this.closeModal.bind(this);
        this.onUpdate = this.onUpdate.bind(this);

        this.state = {
            showModal: false
        }
    }

    onClick() {

        this.setState({
            showModal: true
        });
    }

    closeModal() {
        this.setState({
            showModal: false
        });
    }

    onUpdate(formData) {

        this.closeModal();

        this.props.resource[this.props.field] = formData;
        this.props.onUpdate(this.props.resource);
    }

    render() {

        return (
            <Button bsStyle="info" bsSize="xsmall" onClick={this.onClick}><Glyphicon glyph="arrow-right"/>

                <Modal show={this.state.showModal}
                       onHide={this.closeModal}>

                    <Modal.Header closeButton>
                        <Modal.Title>Update {this.props.title}</Modal.Title>
                    </Modal.Header>
                    <Modal.Body>
                        <FieldBreadcrumb breadcrumb={this.props.breadcrumb}
                                         field={this.props.field} />

                        <ResourceForm schema={this.props.schema}
                                      formData={this.props.formData}
                                      restApi={this.props.restApi}
                                      complete={true}
                                      submitButton="Update"
                                      bsStyle="warning"
                                      metadatas={this.props.metadatas}

                                      onSubmit={this.onUpdate}
                                      onCancel={this.closeModal}
                                      onLoading={this.props.onLoading}
                                      onError={this.props.onError}/>
                    </Modal.Body>
                </Modal>
            </Button>
        );
    }
}

export default FieldLink;