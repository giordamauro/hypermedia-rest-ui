import React, {Component} from "react";
import {Button, ButtonToolbar, Glyphicon, Modal} from "react-bootstrap";

import SelectResourceTable from "../table/SelectResourceTable";
import FieldBreadcrumb from "./FieldBreadcrumb";

class HrefLink extends Component {

    constructor(props) {
        super(props);

        this.onClick = this.onClick.bind(this);
        this.closeModal = this.closeModal.bind(this);
        this.onSelect = this.onSelect.bind(this);
        this.onClear = this.onClear.bind(this);
        this.onSubmit = this.onSubmit.bind(this);

        this.state = {
            showModal: false
        };

        this.selectedHref = null;
    }

    onClick() {

        this.setState({
            showModal: true
        });
    }

    closeModal() {
        this.setState({
            showModal: false
        });
    }

    onClear() {
        this.selectedHref = null;
    }

    onSelect(href) {
        this.selectedHref = href;
    }

    onSubmit() {

        if(this.selectedHref || window.confirm("Select EMTPY?")) {

            this.closeModal();

            const formData = {};
            formData[this.props.field] = this.selectedHref;

            this.props.onUpdate(formData);
        }
    }

    render() {

        if (this.props.href !== this.selectedHref) {
            this.selectedHref = this.props.href;
        }

        return (
            <Button bsStyle="success" bsSize="xsmall" onClick={this.onClick}><Glyphicon glyph="share-alt"/>

                <Modal show={this.state.showModal}
                       bsSize="large"

                       onHide={this.closeModal}>

                    <Modal.Header closeButton>
                        <Modal.Title>Select {this.props.title}</Modal.Title>
                    </Modal.Header>
                    <Modal.Body>
                        <FieldBreadcrumb breadcrumb={this.props.breadcrumb}
                                         field={this.props.field} />

                        <SelectResourceTable restResource={this.props.restResource}
                                             href={this.selectedHref}
                                             refName={this.props.refName}
                                             field={this.props.field}
                                             disabled={this.props.disabled}
                                             metadatas={this.props.metadatas}

                                             onClear={this.onClear}
                                             onSelect={this.onSelect}
                                             onLoading={this.props.onLoading}
                                             onError={this.props.onError}/>

                        <ButtonToolbar>
                            <Button bsStyle="warning" onClick={this.onSubmit} disabled={this.props.disabled}>Select</Button>
                            <Button bsStyle="default" onClick={this.closeModal}>Cancel</Button>
                        </ButtonToolbar>
                    </Modal.Body>
                </Modal>
            </Button>
        )
    }
}

export default HrefLink;