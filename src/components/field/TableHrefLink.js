import React, {Component} from "react";
import update from "react-addons-update";
import {Button, ButtonToolbar, Glyphicon, Modal} from "react-bootstrap";

import HrefArrayTable from "../table/HrefArrayTable";
import ResourceUtil from "../../lib/ResourceUtil";
import FieldBreadcrumb from "./FieldBreadcrumb";

class TableHrefLink extends Component {

    constructor(props) {
        super(props);

        this.onClick = this.onClick.bind(this);
        this.loadResources = this.loadResources.bind(this);
        this.closeModal = this.closeModal.bind(this);
        this.onChange = this.onChange.bind(this);
        this.onSubmit = this.onSubmit.bind(this);

        this.state = {
            showModal: false,
            title: null,
            headers: [],
            resources: []
        };
    }

    onClick() {

        const pagedTable = this;

        this.props.onLoading(true);
        this.props.restResource.getResourceMetadata(metadata => {

            pagedTable.metadata = metadata;

            const schemaProperties = metadata.jsonSchema.properties;

            const headers = ResourceUtil.getNonObjectHeaders(schemaProperties);

            const newState = update(this.state, {
                showModal: {$set: true},
                title: {$set: metadata.jsonSchema.title},
                headers: {$set: headers}
            });
            this.setState(newState);
            this.props.onLoading(false);
        }, this.props.onError);

        this.loadResources();
    }

    closeModal() {

        const newState = update(this.state, {
            showModal: {$set: false},
        });
        this.setState(newState);
    }

    onChange(resources) {

        const newState = update(this.state, {
            resources: {$set: resources},
        });
        this.setState(newState);
    }

    loadResources() {

        this.props.onLoading(true);
        this.props.restResource.restClient.follow(this.props.href, resourcesData => {

            const resources = resourcesData._embedded[this.props.restResource.resourceName];

            const newState = update(this.state, {
                resources: {$set: resources},
            });
            this.setState(newState);
            this.props.onLoading(false);
        }, this.props.onError);
    }

    onSubmit() {
        this.closeModal();

        const links = this.state.resources.map(resource => ResourceUtil.getSelfHref(resource));

        const formData = {};
        formData[this.props.field] = links;

        this.props.onUpdate(formData, this.props.field);
    }

    render() {

        return (
            <Button bsStyle="success" bsSize="xsmall" onClick={this.onClick}><Glyphicon glyph="share-alt"/>

                <Modal show={this.state.showModal}
                       bsSize="large"

                       onHide={this.closeModal}>

                    <Modal.Header closeButton>
                        <Modal.Title>Select {this.props.title} list</Modal.Title>
                    </Modal.Header>
                    <Modal.Body>
                        <FieldBreadcrumb breadcrumb={this.props.breadcrumb}
                                         field={this.props.field} />

                        <HrefArrayTable resources={this.state.resources}
                                        restResource={this.props.restResource}
                                        headers={this.state.headers}
                                        metadatas={this.props.metadatas}

                                        onChange={this.onChange}
                                        onLoading={this.props.onLoading}
                                        onError={this.props.onError}/>

                        <ButtonToolbar>
                            <Button bsStyle="warning" onClick={this.onSubmit}>Update</Button>
                            <Button bsStyle="default" onClick={this.closeModal}>Cancel</Button>
                        </ButtonToolbar>
                    </Modal.Body>
                </Modal>
            </Button>
        )
    }
}

export default TableHrefLink;