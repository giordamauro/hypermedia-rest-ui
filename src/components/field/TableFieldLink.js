import React, {Component} from "react";
import {Button, ButtonToolbar, Glyphicon, Modal} from "react-bootstrap";

import ArrayTable from "../table/ArrayTable";
import FieldBreadcrumb from "./FieldBreadcrumb";

class TableFieldLink extends Component {

    constructor(props) {
        super(props);

        this.onClick = this.onClick.bind(this);
        this.closeModal = this.closeModal.bind(this);
        this.onChange = this.onChange.bind(this);
        this.onSubmit = this.onSubmit.bind(this);
        this.onUpdateLink = this.onUpdateLink.bind(this);

        this.state = {
            showModal: false
        };

        this.resources = [];
    }

    onClick() {

        this.setState({
            showModal: true
        });
    }

    closeModal() {
        this.setState({
            showModal: false
        });
    }

    onChange(resources) {
        this.resources = resources;
    }

    onSubmit() {
        this.closeModal();

        let fieldValue = this.resources;

        const schemaUi = this.props.schema.ui;
        if (schemaUi && schemaUi === "string-array") {

            fieldValue = [];
            this.resources.forEach(value => fieldValue.push(value.value));
        }

        this.props.resource[this.props.field] = fieldValue;
        this.props.onUpdate(this.props.resource);
    }

    onUpdateLink(linkData) {

        const formData = {};
        formData[this.props.field] = linkData;

        this.props.onUpdateLink(formData);
    }

    render() {

        this.resources = this.props.resource[this.props.field];
        const schemaUi = this.props.schema.ui;
        if (schemaUi && schemaUi === "string-array") {

            const arrayResources = [];
            this.resources.forEach(value => arrayResources.push({value: value}));
            this.resources = arrayResources;
        }

        this.breadcrumb = [].concat(this.props.breadcrumb);
        this.breadcrumb.push(this.props.field);

        return (
            <Button bsStyle="info" bsSize="xsmall" onClick={this.onClick}><Glyphicon glyph="arrow-right"/>

                <Modal show={this.state.showModal}
                       bsSize="large"

                       onHide={this.closeModal}>

                    <Modal.Header closeButton>
                        <Modal.Title>Update {this.props.title} list</Modal.Title>
                    </Modal.Header>
                    <Modal.Body>
                        <FieldBreadcrumb breadcrumb={this.props.breadcrumb}
                                         field={this.props.field} />

                        <ArrayTable schema={this.props.schema}
                                    resources={this.resources}
                                    restApi={this.props.restApi}
                                    breadcrumb={this.breadcrumb}
                                    metadatas={this.props.metadatas}

                                    onChange={this.onChange}
                                    onUpdateLink={this.onUpdateLink}
                                    onLoading={this.props.onLoading}
                                    onError={this.props.onError}/>

                        <ButtonToolbar>
                            <Button bsStyle="warning" onClick={this.onSubmit}>Update</Button>
                            <Button bsStyle="default" onClick={this.closeModal}>Cancel</Button>
                        </ButtonToolbar>
                    </Modal.Body>
                </Modal>
            </Button>
        );
    }
}

export default TableFieldLink;