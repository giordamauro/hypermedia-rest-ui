import React, {Component} from "react";
import {Breadcrumb} from "react-bootstrap";

class FieldBreadcrumb extends Component {

    render() {

        let breadcrumb = null;

        if (this.props.breadcrumb) {

            this.breadcrumb = [].concat(this.props.breadcrumb);
            this.breadcrumb.push(this.props.field);

            const breadcrumbItems = [];

            let index = 0;
            this.breadcrumb.forEach(value => {
                breadcrumbItems.push(<Breadcrumb.Item key={this.props.field  + value + index} active>{value}</Breadcrumb.Item>);
                index++;
            });

            breadcrumb = <Breadcrumb>{breadcrumbItems}</Breadcrumb>;
        }

        return breadcrumb;
    }
}

export default FieldBreadcrumb;