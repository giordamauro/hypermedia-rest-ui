import React, {Component} from "react";
import {Button, ButtonToolbar, Glyphicon, Modal} from "react-bootstrap";

import SelectResourceTable from "../table/SelectResourceTable";
import ModalBreadcrumb from "./ModalBreadcrumb"

class AddLinkModal extends Component {

    constructor(props) {
        super(props);

        this.onClick = this.onClick.bind(this);
        this.closeModal = this.closeModal.bind(this);
        this.onSelect = this.onSelect.bind(this);
        this.onClear = this.onClear.bind(this);
        this.onSubmit = this.onSubmit.bind(this);

        this.state = {
            showModal: false
        };

        this.selectedHref = null;
        this.resource = null;
    }

    onClick() {

        this.setState({
            showModal: true
        });
    }

    closeModal() {
        this.setState({
            showModal: false
        });
    }

    onClear() {
        this.selectedHref = null;
    }

    onSelect(href, resource) {
        this.selectedHref = href;
        this.resource = resource;
    }

    onSubmit() {

        if(this.selectedHref) {

            this.closeModal();
            this.props.onSubmit(this.selectedHref, this.resource);
        }
    }

    render() {

        if (this.props.href !== this.selectedHref) {
            this.selectedHref = this.props.href;
        }

        return (
            <Button className="pull-right" bsStyle="primary" bsSize="small" onClick={this.onClick}>
                <Glyphicon glyph="plus"/>

                <Modal show={this.state.showModal}
                       bsSize="large"

                       onHide={this.closeModal}>

                    <Modal.Header closeButton>
                        <Modal.Title>Select one</Modal.Title>
                    </Modal.Header>
                    <Modal.Body>
                        <ModalBreadcrumb breadcrumb={this.props.breadcrumb}/>

                        <SelectResourceTable restResource={this.props.restResource}
                                             href={this.selectedHref}
                                             refName={this.resource}
                                             field={this.props.field}
                                             metadatas={this.props.metadatas}

                                             onClear={this.onClear}
                                             onSelect={this.onSelect}
                                             onLoading={this.props.onLoading}
                                             onError={this.props.onError}/>

                        <ButtonToolbar>
                            <Button bsStyle="warning" onClick={this.onSubmit}>Select</Button>
                            <Button bsStyle="default" onClick={this.closeModal}>Cancel</Button>
                        </ButtonToolbar>
                    </Modal.Body>
                </Modal>
            </Button>
        )
    }
}

export default AddLinkModal;