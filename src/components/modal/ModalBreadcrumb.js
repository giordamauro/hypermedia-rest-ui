import React, {Component} from "react";
import {Breadcrumb} from "react-bootstrap";

class ModalBreadcrumb extends Component {

    render() {

        let breadcrumb = null;

        if (this.props.breadcrumb) {

            const breadcrumbItems = [];

            let index = 0;
            this.props.breadcrumb.forEach(value => {
                breadcrumbItems.push(<Breadcrumb.Item key={value + index} active>{value}</Breadcrumb.Item>);
                index++;
            });
            breadcrumb = <Breadcrumb>{breadcrumbItems}</Breadcrumb>;
        }

        return breadcrumb;
    }
}

export default ModalBreadcrumb;