import React, {Component} from "react";
import {Modal} from "react-bootstrap";
import MDSpinner from "react-md-spinner";

class SpinnerModal extends Component {

    render() {
        return (
            <Modal show={this.props.show} bsSize="small" dialogClassName="spinner-modal">
                <Modal.Body>
                    <MDSpinner/>
                </Modal.Body>
            </Modal>
        );
    }
}
export default SpinnerModal;