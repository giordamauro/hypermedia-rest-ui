import React, {Component} from "react";
import {Button, Glyphicon, Modal} from "react-bootstrap";

import ResourceForm from "../form/ResourceForm";
import ModalBreadcrumb from "./ModalBreadcrumb"

class UpdateModal extends Component {

    constructor(props) {
        super(props);

        this.onClick = this.onClick.bind(this);
        this.closeModal = this.closeModal.bind(this);
        this.onUpdate = this.onUpdate.bind(this);

        this.state = {
            showModal: false
        }
    }

    onClick() {

        this.setState({
            showModal: true
        });
    }

    closeModal() {
        this.setState({
            showModal: false
        });
    }

    onUpdate(formData) {

        this.closeModal();
        this.props.onUpdate(this.props.id, formData, this.props.position);
    }

    render() {

        return (
            <Button bsStyle="warning" bsSize="xsmall" onClick={this.onClick}> <Glyphicon glyph="pencil"/>

                <div className="static-modal">
                    <Modal show={this.state.showModal}
                           onHide={this.closeModal}>

                        <Modal.Header closeButton>
                            <Modal.Title>Update {this.props.schema.title}</Modal.Title>
                        </Modal.Header>
                        <Modal.Body>
                            <ModalBreadcrumb breadcrumb={this.props.breadcrumb}/>

                            <ResourceForm schema={this.props.schema}
                                          formData={this.props.formData}
                                          restApi={this.props.restApi}
                                          metadatas={this.props.metadatas}
                                          submitButton="Update"
                                          bsStyle="warning"

                                          onSubmit={this.onUpdate}
                                          onCancel={this.closeModal}
                                          onLoading={this.props.onLoading}
                                          onError={this.props.onError}/>
                        </Modal.Body>
                    </Modal>
                </div>
            </Button>
        );
    }
}
export default UpdateModal;