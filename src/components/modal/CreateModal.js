import React, {Component} from "react";
import {Button, Glyphicon, Modal} from "react-bootstrap";

import ResourceForm from "../form/ResourceForm";
import ModalBreadcrumb from "./ModalBreadcrumb";

class CreateModal extends Component {

    constructor(props) {
        super(props);

        this.onAdd = this.onAdd.bind(this);
        this.closeModal = this.closeModal.bind(this);
        this.onCreate = this.onCreate.bind(this);

        this.state = {
            showModal: false
        }
    }

    onAdd() {

        this.setState({
            showModal: true
        });
    }

    closeModal() {
        this.setState({
            showModal: false
        });
    }

    onCreate(formData) {

        this.closeModal();
        this.props.onCreate(formData);
    }

    render() {

        return (

            <Button className="pull-right" bsStyle="primary" bsSize="small" onClick={this.onAdd}>
                <Glyphicon glyph="plus"/>

                <Modal show={this.state.showModal}
                       onHide={this.closeModal}>

                    <Modal.Header closeButton>
                        <Modal.Title>Create  {this.props.schema.title}</Modal.Title>
                    </Modal.Header>
                    <Modal.Body>
                        <ModalBreadcrumb breadcrumb={this.props.breadcrumb}/>

                        <ResourceForm schema={this.props.schema}
                                      formData={this.props.formData}
                                      restApi={this.props.restApi}
                                      metadatas={this.props.metadatas}
                                      complete={true}
                                      submitButton="Create"
                                      bsStyle="info"

                                      onSubmit={this.onCreate}
                                      onCancel={this.closeModal}
                                      onLoading={this.props.onLoading}
                                      onError={this.props.onError}/>
                    </Modal.Body>
                </Modal>
            </Button>
        );
    }
}
export default CreateModal;