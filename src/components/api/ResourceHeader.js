import React, {Component} from "react";
import {Button, FormControl, FormGroup, Navbar, Glyphicon, Form} from "react-bootstrap";

class ResourceHeader extends Component {

    constructor(props) {
        super(props);

        this.onChange = this.onChange.bind(this);
        this.onHeadersRefresh = this.onHeadersRefresh.bind(this);

        this.state = {value: ''};
    }

    onChange(event) {
        this.setState({value: event.target.value});
    }

    onHeadersRefresh(event) {
        event.preventDefault();

        const headersObj = {};

        this.state.value.split(', ').forEach(property => {

            const headerSplit = property.split(': ');
            headersObj[headerSplit[0]] = headerSplit[1];
        });

        this.props.onHeadersRefresh(headersObj);
    }

    render() {

        return (
            <Navbar.Form pullLeft>
                <Form inline>
                    <FormGroup>
                        <FormControl style={{width:300}}
                                     type="text"
                                     placeholder="headers"
                                     value={this.state.value}
                                     onChange={this.onChange}/>
                    </FormGroup>
                    {' '}
                    <Button type="submit" bsStyle="default" onClick={this.onHeadersRefresh}>
                        <Glyphicon glyph="pushpin" />
                    </Button>
                </Form>
            </Navbar.Form>
        );
    }
}
export default ResourceHeader;
