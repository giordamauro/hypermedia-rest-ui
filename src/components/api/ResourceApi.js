import React, {Component} from "react";
import update from "react-addons-update";
import {Button, FormControl, FormGroup, Glyphicon, MenuItem, Navbar, NavDropdown, Form} from "react-bootstrap";

import RestApi from "../../lib/RestApi";
import ResourceHeader from "./ResourceHeader";

class ResourceApi extends Component {

    static DEFAULT_API_URL = 'http://localhost:8080/';
    static DEFAULT_TITLE = 'Resources';

    constructor(props) {
        super(props);

        this.onApiUrl = this.onApiUrl.bind(this);
        this.onChange = this.onChange.bind(this);

        this.onResourceSelect = this.onResourceSelect.bind(this);
        this.onHeaderChange = this.onHeaderChange.bind(this);

        this.loadApiResources = this.loadApiResources.bind(this);

        this.restApi = new RestApi(ResourceApi.DEFAULT_API_URL);
        this.headers = {};

        this.state = {
            value: ResourceApi.DEFAULT_API_URL,
            resourceTitle: ResourceApi.DEFAULT_TITLE,
            resources: []
        };
    }

    onChange(event) {
        this.setState({value: event.target.value});
    }

    onApiUrl(event) {

        if(event) {
            event.preventDefault();
        }

        const newState = update(this.state, {
            resourceTitle: {$set: ResourceApi.DEFAULT_TITLE}
        });
        this.setState(newState);

        const normalizedApiUrl = RestApi.normalizeApiUrl(this.state.value);
        const restApi = new RestApi(normalizedApiUrl);
        restApi.setHeaders(this.headers);

        this.restApi = restApi;
        this.loadApiResources();
    }

    onResourceSelect(event) {

        const resource = event.target.text;

        if(this.state.resourceTitle !== resource) {

            this.setState({resourceTitle: resource});
            this.props.onResourceUrl(resource);
        }
    }

    onHeaderChange(headers) {

        this.restApi.setHeaders(headers);
        this.headers = headers;

        this.onApiUrl();
    }

    loadApiResources() {

        const apiResource = this;

        this.props.onApiUrl();
        this.restApi.getResources(resources => {

            const newState = update(this.state, {
                resources: {$set: resources.sort()}
            });
            this.setState(newState);

            apiResource.props.onResources(apiResource.restApi, resources);

        }, this.props.onError);
    }

    render() {

        const resources = [];
        if (this.state.resources) {
            this.state.resources.forEach(resource => {

                resources.push(<MenuItem key={resource} eventKey={resource}
                                         onClick={this.onResourceSelect}>{resource}</MenuItem>);
            });
        }

        const navDropDown = (resources.length > 0) ?
            <NavDropdown eventKey="4" title={this.state.resourceTitle} id="nav-dropdown">
                {resources}
            </NavDropdown>
            :
            null;

        return (
            <div id="navbar" className="navbar-collapse collapse">

                <ul className="nav navbar-nav navbar-left">
                    <Navbar.Form pullLeft>
                        <Form inline>
                            <FormGroup>
                                <FormControl style={{width:250}}
                                             type="text"
                                             placeholder="api url"
                                             value={this.state.value}
                                             onChange={this.onChange}/>
                            </FormGroup>
                            {' '}
                            <Button type="submit" bsStyle="primary" onClick={this.onApiUrl} >
                                <Glyphicon glyph="refresh"/>
                            </Button>
                        </Form>
                    </Navbar.Form>
                    {navDropDown}
                </ul>
                <ul className="nav navbar-nav navbar-right">

                    <ResourceHeader onHeadersRefresh={this.onHeaderChange}/>
                </ul>
            </div>
        );
    }
}
export default ResourceApi;
