import React, {Component} from "react";
import {Grid, Navbar} from "react-bootstrap";
import update from "react-addons-update";

import ResourceApi from "./api/ResourceApi";
import ResourceUtil from "../lib/ResourceUtil";
import PagedResourceTable from "./table/PagedResourceTable";
import SpinnerModal from './modal/SpinnerModal'

class App extends Component {

    constructor(props) {
        super(props);

        this.onResourceUrl = this.onResourceUrl.bind(this);
        this.onApiUrl = this.onApiUrl.bind(this);
        this.onResources = this.onResources.bind(this);
        this.onLoading = this.onLoading.bind(this);
        this.onError = this.onError.bind(this);

        this.metadatas = {};

        this.state = {
            resource: null,
            metadatas: null,
            loading: false,
            loadingApiResource: true
        };
    }

    onApiUrl() {

        const newState = update(this.state, {
            resource: {$set: null},
            loading: {$set: true},
            loadingApiResource: {$set: true}
        });
        this.setState(newState);
    }

    onResourceUrl(resourceName) {

        const newState = update(this.state, {
            resource: {$set: resourceName},
            metadatas: {$set: this.metadatas},
            loading: {$set: true},
            loadingApiResource: {$set: true}
        });
        this.setState(newState);
    }

    onLoading(value) {

        if(this.state.loading !== value) {

            const loadingApi = (value) ? this.state.loadingApiResource : false;
            const newState = update(this.state, {
                loading: {$set: value},
                loadingApiResource : {$set: loadingApi}
            });
            this.setState(newState);
        }
    }

    onError(error) {

        console.error(error);
        this.onLoading(false);

        let readableError = (typeof error === 'object') ? JSON.stringify(error, null, 2) : error;
        alert(readableError);
    }

    onResources(restApi, resources) {

        const newState = update(this.state, {
            resource: {$set: null},
            metadatas: {$set: null}
        });
        this.setState(newState);

        const app = this;
        let resourceCount = 1;

        this.onLoading(true);
        resources.forEach(resource => {

            const resourceMetadata = {};

            const restResource = restApi.getResource(resource);
            resourceMetadata.restResource = restResource;

            restResource.getResourceMetadata(metadata => {

                const schema = metadata.jsonSchema;
                schema.descriptors = metadata.alps.descriptors[0].descriptors;

                resourceMetadata.metadata = metadata;
                resourceMetadata.headers = ResourceUtil.getHeaders(schema);
                resourceMetadata.simpleHeaders = ResourceUtil.getNonObjectHeaders(schema.properties);
                resourceMetadata.title = metadata.jsonSchema.title;

                if(resourceCount < resources.length) {
                    resourceCount++;
                }
                else {
                    app.onLoading(false);
                }

            }, this.props.onError);

            app.metadatas[resource] = resourceMetadata;
        });
    }

    render() {

        const showResource = (this.state.resource && !this.state.loadingApiResource) ? {} : {display: 'none'};

        return (
            <div className="App">
                <Navbar bsStyle="inverse" staticTop>
                    <Grid fluid={true}>
                        <Navbar.Header>
                            <Navbar.Brand>
                                <a href="." style={{color: 'white'}}>REST Hypermedia - React UI</a>
                            </Navbar.Brand>
                        </Navbar.Header>
                        <ResourceApi onApiUrl={this.onApiUrl}
                                     onResources={this.onResources}
                                     onResourceUrl={this.onResourceUrl}

                                     onLoading={this.onLoading}
                                     onError={this.onError}/>
                    </Grid>
                </Navbar>
                <SpinnerModal show={this.state.loading} />
                <Grid fluid={true} style={showResource}>
                        <PagedResourceTable resource={this.state.resource}
                                            metadatas={this.state.metadatas}

                                            onLoading={this.onLoading}
                                            onError={this.onError}/>
                </Grid>
            </div>
        );
    }
}
export default App;
