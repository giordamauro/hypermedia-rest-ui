import React, {Component} from "react";
import {Table} from "react-bootstrap";

import SelectRow from "./SelectRow";
import AddLinkModal from "../modal/AddLinkModal";
import ResourceUtil from "../../lib/ResourceUtil";

class HrefArrayTable extends Component {

    constructor(props) {
        super(props);

        this.onDelete = this.onDelete.bind(this);
        this.onCreate = this.onCreate.bind(this);

        this.resources = [];
    }

    onDelete(resourceId) {

        this.resources.splice(resourceId, 1);
        this.props.onChange(this.resources);
    }

    onCreate(href, resource) {

        this.resources.push(resource);
        this.props.onChange(this.resources);
    }

    render() {

        if (this.props.resources !== this.resources) {
            this.resources = this.props.resources;
        }

        const headers = [];

        this.props.headers.forEach(header => {
            headers.push(<th className="text-center" key={header.key}>{header.value.title}</th>);
        });

        const resources = [];

        let index = 0;
        this.props.resources.forEach(resource => {

            const selfHref = ResourceUtil.getSelfHref(resource);

            resources.push(<SelectRow key={index}
                                      href={selfHref}
                                      resource={resource}
                                      headers={this.props.headers}
                                      selected={true}

                                      onClear={this.onDelete}/>);
            index++;
        });

        return (
            <div>
                <Table striped bordered condensed hover>
                    <thead className="thead-default">
                    <tr>
                        {headers}
                        <th className="text-center"/>
                    </tr>
                    </thead>
                    <tbody className="table-striped">
                    {resources}
                    </tbody>
                </Table>
                <AddLinkModal restResource={this.props.restResource}
                              metadatas={this.props.metadatas}

                              onSubmit={this.onCreate}
                              onLoading={this.props.onLoading}
                              onError={this.props.onError}/>
            </div>
        )
    }
}
export default HrefArrayTable;