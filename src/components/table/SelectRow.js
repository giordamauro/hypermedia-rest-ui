import React, {Component} from "react";
import {Button, ButtonToolbar, Checkbox, Glyphicon} from "react-bootstrap";

class TableRow extends Component {

    constructor(props) {
        super(props);

        this.onClear = this.onClear.bind(this);
        this.onSelect = this.onSelect.bind(this);

        this.appendCells = this.appendCells.bind(this);
    }

    onClear() {
        this.props.onClear();
    }

    onSelect() {
        this.props.onSelect(this.props.href, this.props.resource);
    }

    appendCells(cells, headers, resource) {

        headers.forEach(header => {

            let fieldData = resource[header.key];

            if (header.value.type === 'boolean' && fieldData !== undefined && fieldData !== null) {
                fieldData = <Checkbox disabled={true} inline={true} checked={fieldData}/>
            }

            cells.push(<td style={{textAlign: 'center'}} key={header.key}>{fieldData}</td>);
        });
    }

    render() {

        const cells = [];
        this.appendCells(cells, this.props.headers, this.props.resource);

        const button = (this.props.selected) ?
            <Button bsStyle="danger" bsSize="xsmall" onClick={this.onClear} disabled={this.props.disabled}><Glyphicon glyph="remove"/></Button>
            :
            <Button bsStyle="info" bsSize="xsmall" onClick={this.onSelect}><Glyphicon glyph="ok"/></Button>

        return (
            <tr>
                {cells}

                <td key="actionButtons">
                    <ButtonToolbar className="pull-right">
                        {button}
                    </ButtonToolbar>
                </td>
            </tr>
        );
    }
}
export default TableRow;