import React, {Component} from "react";
import {Table} from "react-bootstrap";

import TableRow from "./TableRow";
import CreateModal from "../modal/CreateModal";
import ResourceUtil from "../../lib/ResourceUtil";

class ArrayTable extends Component {

    constructor(props) {
        super(props);

        this.onDelete = this.onDelete.bind(this);
        this.onCreate = this.onCreate.bind(this);
        this.onUpdate = this.onUpdate.bind(this);
        this.onUpdateLink = this.onUpdateLink.bind(this);

        this.resources = [];
    }

    onDelete(resourceId) {

        const confirmed = window.confirm("Confirm DELETE?");
        if (confirmed) {
            this.resources.splice(resourceId, 1);

            this.forceUpdate();
            this.props.onChange(this.resources);
        }
    }

    onCreate(formData) {

        this.resources.push(formData);
        this.forceUpdate();
        this.props.onChange(this.resources);
    }

    onUpdate(resourceId, formData) {

        this.resources[resourceId] = formData;
        this.forceUpdate();
        this.props.onChange(this.resources);
    }

    onUpdateLink(id, linkData) {

        const formData = [];
        for(let i = 0; i < id; i++) {
            formData.push({});
        }

        formData[id-1] = linkData;
        this.props.onUpdateLink(formData);
    }


    render() {

        if(this.props.resources !== this.resources) {
            this.resources = this.props.resources;
        }

        const headers = [];

        const fieldHeaders = ResourceUtil.getHeaders(this.props.schema);

        fieldHeaders.forEach(header => {
            headers.push(<th className="text-center" key={header.key}>{header.value.title}</th>);
        });

        const resources = [];
        let index = 0;
        this.resources.forEach(resource => {

            resources.push(<TableRow key={index}
                                     position={index}
                                     id={index}
                                     schema={this.props.schema}
                                     resource={resource}
                                     headers={fieldHeaders}
                                     restApi={this.props.restApi}
                                     breadcrumb={this.props.breadcrumb}

                                     onUpdate={this.onUpdate}
                                     onUpdateLink={this.onUpdateLink}
                                     onDelete={this.onDelete}
                                     onLoading={this.props.onLoading}
                                     onError={this.props.onError}/>);
            index++;
        });

        return (
            <div>
                <Table striped bordered condensed hover>
                    <thead className="thead-default">
                    <tr>
                        {headers}
                        <th className="text-center"/>
                    </tr>
                    </thead>
                    <tbody className="table-striped">
                    {resources}
                    </tbody>
                </Table>
                <CreateModal schema={this.props.schema}
                             restApi={this.props.restApi}
                             breadcrumb={this.props.breadcrumb}

                             onCreate={this.onCreate}
                             onLoading={this.props.onLoading}
                             onError={this.props.onError}/>
            </div>
        )
    }
}

export default ArrayTable;