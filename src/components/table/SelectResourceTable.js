import React, {Component} from "react";
import update from "react-addons-update";
import {ButtonGroup, Table} from "react-bootstrap";

import TableNavigation from "./TableNavigation";
import ResourceUtil from "../../lib/ResourceUtil";
import SelectRow from "./SelectRow";
import Search from "./Search";

class SelectResourceTable extends Component {

    constructor(props) {
        super(props);

        this.loadResources = this.loadResources.bind(this);
        this.onNavigate = this.onNavigate.bind(this);
        this.onClear = this.onClear.bind(this);
        this.onSelect = this.onSelect.bind(this);
        this.onSearch = this.onSearch.bind(this);

        this.pageNumber = 0;
        this.pageSize = 10;

        this.restResource = null;
        this.metadata = null;

        this.state = {
            resources: [],
            selectedResource: null,
            navLinks: {},
            page: null
        };
    }

    loadResources() {

        const pagedTable = this;

        this.props.onLoading(true);
        this.restResource.getPaged(this.pageNumber, this.pageSize, resources => {

            const resourceName = this.restResource.resourceName;
            const tableResources = resources._embedded[resourceName];

            const navLinks = resources._links;
            pagedTable.page = resources.page;

            const newState = update(this.state, {
                resources: {$set: tableResources},
                selectedResource: {$set: null},
                navLinks: {$set: navLinks},
                page: {$set: pagedTable.page}
            });
            this.setState(newState);
            this.props.onLoading(false);
        }, this.props.onError);
    }

    loadSelectedResource() {

        this.props.onLoading(true);
        this.restResource.restClient.follow(this.props.href, resource => {

            const newState = update(this.state, {
                resources: {$set: []},
                selectedResource: {$set: resource},
                navLinks: {$set: {}},
                page: {$set: null}
            });
            this.setState(newState);
            this.props.onLoading(false);
        }, error => {
            this.loadResources();
        });
    }

    onSearch(query) {

        this.pageNumber = 0;

        const pagedTable = this;

        this.props.onLoading(true);
        this.restResource.getPagedQuery(this.pageNumber, this.pageSize, query, resources => {

            const resourceName = this.restResource.resourceName;
            const tableResources = resources._embedded[resourceName];

            const navLinks = resources._links;
            pagedTable.page = resources.page;

            const newState = update(this.state, {
                resources: {$set: tableResources},
                selectedResource: {$set: null},
                navLinks: {$set: navLinks},
                page: {$set: pagedTable.page}
            });
            this.setState(newState);
            this.props.onLoading(false);
        }, this.props.onError);
    }

    onNavigate(navigation) {

        if (navigation === 'first') {
            this.pageNumber = 0;
        }
        else if (navigation === 'next') {
            this.pageNumber++;
        }
        else if (navigation === 'prev') {
            this.pageNumber--;
        }
        else if (navigation === 'last') {
            this.pageNumber = (this.page) ? this.page.totalPages - 1 : 0;
        }

        this.loadResources();
    }

    onClear() {
        this.onNavigate('first');

        this.props.onClear();
    }

    onSelect(href, resource) {

        const newState = update(this.state, {
            resources: {$set: []},
            selectedResource: {$set: resource},
            navLinks: {$set: {}},
            page: {$set: null}
        });
        this.setState(newState);

        this.props.onSelect(href, resource);
    }

    render() {

        if (this.props.restResource && this.props.restResource !== this.restResource) {

            this.restResource = this.props.restResource;

            if (this.props.href) {
                this.loadSelectedResource();
            }
            else {
                this.pageNumber = 0;
                this.loadResources();
            }
        }

        const headers = [];
        const metadataHeaders = this.props.metadatas[this.props.refName].simpleHeaders;
        metadataHeaders.forEach(header => {
            headers.push(<th className="text-center" key={header.key}>{header.value.title}</th>);
        });

        const resources = [];

        if (this.state.selectedResource) {

            resources.push(<SelectRow key={0}
                                      href={this.props.href}
                                      resource={this.state.selectedResource}
                                      headers={metadataHeaders}
                                      disabled={this.props.disabled}
                                      selected={true}

                                      onClear={this.onClear}/>);
        }
        else if(!this.props.disabled) {
            let index = 0;
            this.state.resources.forEach(resource => {

                const selfHref = ResourceUtil.getSelfHref(resource);

                resources.push(<SelectRow key={index}
                                          href={selfHref}
                                          resource={resource}
                                          headers={metadataHeaders}
                                          disabled={false}

                                          onSelect={this.onSelect}/>);
                index++;
            });
        }

        return (
            <div>
                <Search onSearch={this.onSearch}/>
                <br/>
                <Table striped bordered condensed hover>
                    <thead className="thead-default">
                    <tr>
                        {headers}
                        <th className="text-center"/>
                    </tr>
                    </thead>
                    <tbody className="table-striped">
                    {resources}
                    </tbody>
                </Table>
                <ButtonGroup className="pull-left">
                    <TableNavigation navLinks={this.state.navLinks}
                                     page={this.state.page}

                                     onNavigate={this.onNavigate}/>
                </ButtonGroup>
            </div>
        )
    }
}

export default SelectResourceTable;