import React, {Component} from "react";
import {Button, ButtonGroup} from "react-bootstrap";

class TableNavigation extends Component {

    constructor(props) {
        super(props);

        this.handleNavFirst = this.handleNavFirst.bind(this);
        this.handleNavPrev = this.handleNavPrev.bind(this);
        this.handleNavNext = this.handleNavNext.bind(this);
        this.handleNavLast = this.handleNavLast.bind(this);
    }

    handleNavFirst(e) {
        e.preventDefault();

        this.props.onNavigate('first');
    }

    handleNavPrev(e) {
        e.preventDefault();

        this.props.onNavigate('prev');
    }

    handleNavNext(e) {
        e.preventDefault();

        this.props.onNavigate('next');
    }

    handleNavLast(e) {
        e.preventDefault();

        this.props.onNavigate('last');
    }

    render() {

        const propsNavLinks = this.props.navLinks;
        const navLinks = [];

        if ("first" in propsNavLinks) {
            navLinks.push(<Button key="first" onClick={this.handleNavFirst}>&lt;&lt;</Button>);
        }
        if ("prev" in propsNavLinks) {
            navLinks.push(<Button key="prev" onClick={this.handleNavPrev}>&lt;</Button>);
        }
        if ("next" in propsNavLinks) {
            navLinks.push(<Button key="next" onClick={this.handleNavNext}>&gt;</Button>);
        }
        if ("last" in propsNavLinks) {
            navLinks.push(<Button key="last" onClick={this.handleNavLast}>&gt;&gt;</Button>);
        }

        const propsPage = this.props.page;
        let pageInfo = "";

        if (propsPage && propsPage.totalElements) {
            const size = (propsPage.size > propsPage.totalElements) ? propsPage.totalElements : propsPage.size;
            pageInfo = <div>
                <strong>{size}</strong>
                {" of "}
                <strong>{propsPage.totalElements}</strong>
                {" - page "}
                {propsPage.number}
                {"  "}
            </div>
        }

        return (
            <div>
                {pageInfo}
                <ButtonGroup>
                    {navLinks}
                </ButtonGroup>
            </div>
        );
    }
}

export default TableNavigation;