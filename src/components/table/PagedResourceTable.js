import React, {Component} from "react";
import update from "react-addons-update";
import {ButtonGroup, Col, Row, Table} from "react-bootstrap";

import TableNavigation from "./TableNavigation";
import TableRow from "./TableRow";
import CreateModal from "../modal/CreateModal";
import RestResourceItem from "../../lib/RestResourceItem";
import ResourceUtil from "../../lib/ResourceUtil";
import Search from "./Search";

class PagedResourceTable extends Component {

    constructor(props) {
        super(props);

        this.loadResources = this.loadResources.bind(this);
        this.onNavigate = this.onNavigate.bind(this);
        this.onDelete = this.onDelete.bind(this);
        this.onCreate = this.onCreate.bind(this);
        this.onUpdate = this.onUpdate.bind(this);
        this.onUpdateLink = this.onUpdateLink.bind(this);
        this.onUpdateLinkArray = this.onUpdateLinkArray.bind(this);
        this.onSearch = this.onSearch.bind(this);

        this.pageNumber = 0;
        this.pageSize = 10;

        this.resource = null;
        this.restResource = null;

        this.state = {
            title: "",
            headers: [],
            resources: [],
            navLinks: {},
            page: null
        };
    }

    loadResources() {

        const pagedTable = this;

        this.props.onLoading(true);
        this.restResource.getPaged(this.pageNumber, this.pageSize, resources => {

            const resourceName = this.restResource.resourceName;
            const tableResources = resources._embedded[resourceName];

            const navLinks = resources._links;
            pagedTable.page = resources.page;

            const newState = update(this.state, {
                resources: {$set: tableResources},
                navLinks: {$set: navLinks},
                page: {$set: pagedTable.page}
            });
            this.setState(newState);
            this.props.onLoading(false);
        }, this.props.onError);
    }

    onSearch(query) {

        this.pageNumber = 0;

        const pagedTable = this;

        this.props.onLoading(true);
        this.restResource.getPagedQuery(this.pageNumber, this.pageSize, query, resources => {

            const resourceName = this.restResource.resourceName;
            const tableResources = resources._embedded[resourceName];

            const navLinks = resources._links;
            pagedTable.page = resources.page;

            const newState = update(this.state, {
                resources: {$set: tableResources},
                navLinks: {$set: navLinks},
                page: {$set: pagedTable.page}
            });
            this.setState(newState);
            this.props.onLoading(false);
        }, this.props.onError);
    }

    onNavigate(navigation) {

        if (navigation === 'first') {
            this.pageNumber = 0;
        }
        else if (navigation === 'next') {
            this.pageNumber++;
        }
        else if (navigation === 'prev') {
            this.pageNumber--;
        }
        else if (navigation === 'last') {
            this.pageNumber = (this.page) ? this.page.totalPages - 1 : 0;
        }

        this.loadResources();
    }

    onDelete(resourceId) {

        const confirmed = window.confirm("Confirm DELETE?");
        if (confirmed) {
            const resourceItem = new RestResourceItem(this.restResource, resourceId);

            this.props.onLoading(true);
            resourceItem.remove(() => {

                this.onNavigate('first');
                this.props.onLoading(false);
            }, this.props.onError);
        }
    }

    onCreate(formData) {

        this.props.onLoading(true);
        this.restResource.create(formData, () => {

            this.onNavigate('last');
            this.props.onLoading(false);
        }, this.props.onError);
    }

    onUpdate(resourceId, formData, position) {

        const resourceItem = new RestResourceItem(this.restResource, resourceId);

        const pagedTable = this;

        this.props.onLoading(true);
        resourceItem.update(formData, resultData => {
            this.props.onLoading(false);

            if(position || position === 0) {

                const newState = update(pagedTable.state, {
                    $apply: state => state.resources[position] = resultData
                });
                pagedTable.setState(newState);
            }
        }, this.props.onError);
    }

    onUpdateLink(resourceId, formData) {

        this.props.onLoading(true);
        const resourceItem = new RestResourceItem(this.restResource, resourceId);
        resourceItem.patch(formData, () => {
            this.props.onLoading(false);
        }, this.props.onError);
    }

    onUpdateLinkArray(resourceId, formData, fieldName) {

        const resourceItem = new RestResourceItem(this.restResource, resourceId);
        this.props.onLoading(true);
        resourceItem.updateRelationship(fieldName, formData[fieldName], () => {
            this.props.onLoading(false);
        }, this.props.onError);
    }

    render() {

        if(!this.props.resource) {
            this.resource = null;   
        }

        if (this.props.resource && this.props.resource !== this.resource) {

            this.resource = this.props.resource;
            this.restResource = this.props.metadatas[this.resource].restResource;
            this.pageNumber = 0;

            this.loadResources();
        }

        const restApi = (this.restResource) ? this.restResource.restApi : null;

        const headers = [];
        const metadataHeaders = (this.props.metadatas && this.resource) ? this.props.metadatas[this.resource].headers : [];

        if(this.props.metadatas && metadataHeaders) {
            metadataHeaders.forEach(header => {
                headers.push(<th className="text-center" key={header.key}>{header.value.title}</th>);
            });
        }

        const jsonSchema = (this.props.metadatas && this.resource) ? this.props.metadatas[this.resource].metadata.jsonSchema : {};

        const resources = [];
        let index = 0;
        this.state.resources.forEach(resource => {

            const selfId = ResourceUtil.getSelfId(resource);

            resources.push(<TableRow key={index}
                                     position={index}
                                     id={selfId}
                                     schema={jsonSchema}
                                     resource={resource}
                                     headers={metadataHeaders}
                                     restApi={restApi}
                                     breadcrumb={[this.restResource.resourceName]}
                                     metadatas={this.props.metadatas}

                                     onUpdate={this.onUpdate}
                                     onUpdateLink={this.onUpdateLink}
                                     onUpdateLinkArray={this.onUpdateLinkArray}
                                     onDelete={this.onDelete}
                                     onLoading={this.props.onLoading}
                                     onError={this.props.onError}/>);
            index++;
        });

        const title = (this.props.metadatas && this.resource) ? this.props.metadatas[this.resource].title : "";

        return (
            <div>
                <Col md={9}>
                    <legend>{title}</legend>
                </Col>
                <Col md={3}>
                    <Search onSearch={this.onSearch}/>
                </Col>
                <Col md={12}>
                    <Row>
                        <Table striped bordered condensed hover>
                            <thead className="thead-default">
                            <tr>
                                {headers}
                                <th className="text-center"/>
                            </tr>
                            </thead>
                            <tbody className="table-striped">
                            {resources}
                            </tbody>
                        </Table>
                    </Row>
                    <Row>
                        <ButtonGroup className="pull-left">
                            <TableNavigation navLinks={this.state.navLinks}
                                             page={this.state.page}

                                             onNavigate={this.onNavigate}/>
                        </ButtonGroup>
                        <CreateModal schema={jsonSchema}
                                     restApi={restApi}
                                     metadatas={this.props.metadatas}

                                     onCreate={this.onCreate}
                                     onLoading={this.props.onLoading}
                                     onError={this.props.onError}/>
                    </Row>
                </Col>
            </div>
        )
    }
}

export default PagedResourceTable;