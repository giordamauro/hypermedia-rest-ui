import React, {Component} from "react";
import {Button, FormControl, FormGroup, Glyphicon, Form} from "react-bootstrap";

class Search extends Component {

    constructor(props) {
        super(props);

        this.onChange = this.onChange.bind(this);
        this.onSearch = this.onSearch.bind(this);

        this.state = {value: ''};
    }

    onChange(event) {
        this.setState({value: event.target.value});
    }

    onSearch(event) {
        event.preventDefault();

        this.props.onSearch(this.state.value);
    }

    render() {

        return (
            <Form inline>
                <FormGroup>
                    <FormControl type="text"
                                 placeholder="search"
                                 value={this.state.value}
                                 onChange={this.onChange}/>
                </FormGroup>
                {' '}
                <Button type="submit" bsStyle="info" onClick={this.onSearch}>
                    <Glyphicon glyph="search" />
                </Button>
            </Form>
        );
    }
}
export default Search;
