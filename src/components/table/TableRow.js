import React, {Component} from "react";
import {Button, ButtonToolbar, Checkbox, Glyphicon} from "react-bootstrap";

import ResourceUtil from "../../lib/ResourceUtil";

import HrefLink from "../field/HrefLink";
import UpdateModal from "../modal/UpdateModal";
import TableFieldLink from "../field/TableFieldLink";
import FieldLink from "../field/FieldLink";
import TableHrefLink from "../field/TableHrefLink";

class TableRow extends Component {

    constructor(props) {
        super(props);

        this.onDelete = this.onDelete.bind(this);
        this.onUpdate = this.onUpdate.bind(this);
        this.onUpdateLink = this.onUpdateLink.bind(this);
        this.onUpdateLinkArray = this.onUpdateLinkArray.bind(this);

        this.appendCells = this.appendCells.bind(this);
        this.getObjectCell = this.getObjectCell.bind(this);
    }

    onDelete() {

        this.props.onDelete(this.props.id);
    }

    onUpdate(formData) {

        this.props.onUpdate(this.props.id, formData, this.props.position);
    }

    onUpdateLink(formData) {

        this.props.onUpdateLink(this.props.id, formData);
    }

    onUpdateLinkArray(formData, fieldName) {

        this.props.onUpdateLinkArray(this.props.id, formData, fieldName);
    }

    appendCells(cells, headers, resource) {

        headers.forEach(header => {

            let fieldData = resource[header.key];

            if (header.type === 'object') {
                fieldData = this.getObjectCell(header, resource);
            }
            else if (header.type === 'ref') {
                fieldData = this.getRefCell(header, resource);
            }
            else {
                if (header.value.type === 'boolean' && fieldData !== undefined && fieldData !== null) {
                    fieldData = <Checkbox disabled={true} inline={true} checked={fieldData}/>
                }
            }
            cells.push(<td style={{textAlign: 'center'}} key={header.key}>{fieldData}</td>);
        });
    }

    getObjectCell(header, resource) {

        return (header.singular) ?

            <FieldLink schema={header.subSchema}
                       field={header.key}
                       resource={resource}
                       formData={resource[header.key]}
                       restApi={this.props.restApi}
                       title={header.value.title}
                       breadcrumb={this.breadcrumb}
                       metadatas={this.props.metadatas}

                       onUpdate={this.onUpdate}
                       onLoading={this.props.onLoading}
                       onError={this.props.onError}/>
            :
            <TableFieldLink schema={header.subSchema}
                            field={header.key}
                            resource={resource}
                            restApi={this.props.restApi}
                            title={header.value.title}
                            breadcrumb={this.breadcrumb}
                            metadatas={this.props.metadatas}

                            onUpdateLink={this.onUpdateLink}
                            onUpdate={this.onUpdate}
                            onLoading={this.props.onLoading}
                            onError={this.props.onError}/>;
    }

    getRefCell(header, resource) {

        const restResource = this.props.restApi.getResource(header.refName);
        const href = ResourceUtil.getFieldHref(resource, header.key);

        const disabled = !ResourceUtil.isEditable(header);

        return (header.singular) ?
            <HrefLink restResource={restResource}
                      field={header.key}
                      refName={header.refName}
                      href={href}
                      title={header.value.title}
                      breadcrumb={this.breadcrumb}
                      disabled={disabled}
                      metadatas={this.props.metadatas}

                      onUpdate={this.onUpdateLink}
                      onLoading={this.props.onLoading}
                      onError={this.props.onError}/>
            :
            <TableHrefLink restResource={restResource}
                           field={header.key}
                           href={href}
                           title={header.value.title}
                           breadcrumb={this.breadcrumb}
                           metadatas={this.props.metadatas}

                           onUpdate={this.onUpdateLinkArray}
                           onLoading={this.props.onLoading}
                           onError={this.props.onError}/>;
    }

    render() {

        this.breadcrumb = [].concat(this.props.breadcrumb);
        const breadcrumbId = (typeof this.props.id === 'string') ? this.props.id : this.props.id + 1;
        this.breadcrumb.push(breadcrumbId);

        const cells = [];
        this.appendCells(cells, this.props.headers, this.props.resource);

        return (
            <tr>
                {cells}

                <td key="actionButtons">
                    <ButtonToolbar className="pull-right">
                        <UpdateModal key="update"
                                     position={this.props.position}
                                     id={this.props.id}
                                     schema={this.props.schema}
                                     formData={this.props.resource}
                                     restApi={this.props.restApi}
                                     breadcrumb={this.breadcrumb}
                                     metadatas={this.props.metadatas}

                                     onUpdate={this.props.onUpdate}
                                     onLoading={this.props.onLoading}
                                     onError={this.props.onError}/>

                        <Button key="delete" bsStyle="danger" bsSize="xsmall" onClick={this.onDelete}> <Glyphicon
                            glyph="trash"/></Button>
                    </ButtonToolbar>
                </td>
            </tr>
        );
    }
}
export default TableRow;