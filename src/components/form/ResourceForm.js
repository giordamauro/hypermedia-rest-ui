import React, {Component} from "react";
import Form from "react-jsonschema-form";
import {Button, ButtonToolbar} from "react-bootstrap";

import ResourceUtil from "../../lib/ResourceUtil";
import HrefLinkWidget from "./HrefLinkWidget";
import TableHrefLinkWidget from "./TableHrefLinkWidget";

class ResourceForm extends Component {

    constructor(props) {
        super(props);

        this.onChange = this.onChange.bind(this);
        this.onSubmit = this.onSubmit.bind(this);

        this.onHrefLink = this.onHrefLink.bind(this);
        this.onHrefLinkArray = this.onHrefLinkArray.bind(this);

        this.fields = {
            hrefLink: HrefLinkWidget,
            tableHrefLink: TableHrefLinkWidget
            // fieldLink: FieldLinkWidget,
            // tableField: TableFieldWidget
        };

        this.formData = {};
        this.uiSchema = {};
    }

    onChange(event) {
        this.formData = event.formData;
    }

    onSubmit(event) {
        this.props.onSubmit(this.formData);
    }

    onHrefLink(formData) {

        if (!this.formData) {
            this.formData = {};
        }

        Object.assign(this.formData, formData);
    }

    onHrefLinkArray(formData, fieldName) {

        if (!this.formData) {
            this.formData = {};
        }

        Object.assign(this.formData, formData);
    }

    getSchema(schema, headerProperties, metadatas) {

        this.uiSchema = {};

        const newSchemaProperties = {};
        const requiredFields = [];

        headerProperties.forEach(header => {

            const headerUiSchema = {};

            if (header.type !== 'object' || (header.type === 'ref' && header.singular)) {

                let value = header.value;
                let hideProperty = false;

                const propertyDetails = header.details;
                if (propertyDetails) {

                    value = {};
                    Object.assign(value, header.value);

                    value.description = (propertyDetails.description) ? propertyDetails.description : '';

                    if (propertyDetails.required) {
                        requiredFields.push(header.key);
                    }

                    if (propertyDetails.readOnly || (propertyDetails.hasOwnProperty("editable") && !propertyDetails.editable && !this.props.complete)) {
                        headerUiSchema['ui:readonly'] = true;
                    }

                    if (propertyDetails.autoFocus) {
                        headerUiSchema['ui:autofocus'] = true;
                    }
                    
                    hideProperty = (propertyDetails.hasOwnProperty("hide") && propertyDetails.hide && this.props.complete);
                }

                if(!hideProperty) {
                    newSchemaProperties[header.key] = value;
                }
            }

            // if (header.value.type === 'array') {
            //
            //     this.uiSchema[header.key] = {
            //         'ui:widget': 'tableField',
            //         "ui:options": {
            //             schema: header.subSchema,
            //             field: header.key,
            //             resource: {},
            //             metadatas: metadatas,
            //
            //             onUpdate: this.onTableFieldUpdate
            //         }
            //     };
            // }
            // else {
            //     this.uiSchema[header.key] = {
            //         'ui:widget': 'fieldLink',
            //         "ui:options": {
            //             schema: header.subSchema,
            //             formData: {},
            //             metadatas: metadatas,
            //
            //             onUpdate: this.onFieldUpdate
            //         }
            //     };
            // }

            if (header.type === 'ref') {

                const restResource = this.props.restApi.getResource(header.refName);

                if (header.singular) {

                    Object.assign(headerUiSchema, {
                        'ui:widget': "hrefLink",
                        'ui:options': {
                            restResource: restResource,
                            field: header.key,
                            refName: header.refName,
                            title: header.value.title,
                            metadatas: metadatas,

                            onUpdate: this.onHrefLink,
                            onLoading: this.props.onLoading,
                            onError: this.props.onError
                        }
                    });
                }
                else {
                    Object.assign(headerUiSchema, {
                        'ui:widget': "tableHrefLink",
                        'ui:options': {
                            restResource: restResource,
                            field: header.key,
                            refName: header.refName,
                            title: header.value.title,
                            metadatas: metadatas,

                            onUpdate: this.onHrefLinkArray,
                            onLoading: this.props.onLoading,
                            onError: this.props.onError
                        }
                    });
                }
            }

            if (Object.keys(headerUiSchema).length !== 0) {
                this.uiSchema[header.key] = headerUiSchema;
            }
        });

        const newSchema = {};
        Object.assign(newSchema, schema);

        newSchema.title = null;
        newSchema['properties'] = newSchemaProperties;
        newSchema['required'] = requiredFields;

        return newSchema;
    }

    render() {

        if (this.props.formData && this.props.formData !== this.formData) {
            this.formData = this.props.formData;
        }

        const style = (this.props.bsStyle) ? this.props.bsStyle : "default";
        const submitText = (this.props.submitButton) ? this.props.submitButton : "submit";

        const headers = (this.props.complete) ?
            ResourceUtil.getHeaders(this.props.schema)
            :
            ResourceUtil.getNonObjectHeaders(this.props.schema.properties);
        const newSchema = this.getSchema(this.props.schema, headers, this.props.metadatas);

        return (
            <div className="schema-form">

                <Form schema={newSchema}
                      formData={this.formData}

                      uiSchema={this.uiSchema}
                      widgets={this.fields}
                      liveValidate={true}

                      onError={this.props.onError}
                      onChange={this.onChange}
                      onSubmit={this.onSubmit}>

                    <ButtonToolbar>
                        <Button bsStyle={style} onClick={this.onSubmit}>{submitText}</Button>
                        <Button bsStyle="default" onClick={this.props.onCancel}>Cancel</Button>
                    </ButtonToolbar>
                </Form>
            </div>
        );
    }
}

export default ResourceForm;
