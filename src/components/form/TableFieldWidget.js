import React, {Component} from "react";

import TableFieldLink from "../field/TableFieldLink";

class TableFieldWidget extends Component {

    render() {

        return (
            <TableFieldLink schema={this.props.options.schema}
                            field={this.props.options.field}

                            resource={this.props.options.resource}
                            metadatas={this.props.options.metadatas}

                            onUpdate={this.props.options.onUpdate}
                            onLoading={this.props.options.onLoading}
                            onError={this.props.options.onError}/>
        );
    }
}

export default TableFieldWidget;