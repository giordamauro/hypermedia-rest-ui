import React, {Component} from "react";

import TableHrefLink from "../field/TableHrefLink";

class TableHrefLinkWidget extends Component {

    render() {

        return (
            <TableHrefLink restResource={this.props.options.restResource}
                           refName={this.props.options.refName}
                           field={this.props.options.field}
                           metadatas={this.props.options.metadatas}

                           onUpdate={this.props.options.onUpdate}
                           onLoading={this.props.options.onLoading}
                           onError={this.props.options.onError}/>
        );
    }
}

export default TableHrefLinkWidget;