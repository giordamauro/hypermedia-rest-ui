import React, {Component} from "react";

import HrefLink from "../field/HrefLink";

class HrefLinkWidget extends Component {

    render() {

        return (
            <HrefLink restResource={this.props.options.restResource}
                      field={this.props.options.field}
                      refName={this.props.options.refName}
                      title={this.props.options.title}
                      disabled={false}
                      metadatas={this.props.options.metadatas}

                      onUpdate={this.props.options.onUpdate}
                      onLoading={this.props.options.onLoading}
                      onError={this.props.options.onError}/>
        );
    }
}

export default HrefLinkWidget;