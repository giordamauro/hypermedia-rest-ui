import React, {Component} from "react";

import FieldLink from "../field/FieldLink";

class FieldLinkWidget extends Component {

    render() {

        return (
            <FieldLink schema={this.props.options.schema}
                       formData={this.props.options.formData}
                       metadatas={this.props.options.metadatas}

                       onClick={this.props.options.onUpdate}
                       onLoading={this.props.options.onLoading}
                       onError={this.props.options.onError}/>
        )
    }
}

export default FieldLinkWidget;